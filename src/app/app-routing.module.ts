import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder',
    pathMatch: 'full'
  },
  {
    path: 'folder',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'ion-loading',
    loadChildren: () => import('./ion-loading/ion-loading.module').then( m => m.IonLoadingPageModule)
  },
  {
    path: 'ion-progress-bar',
    loadChildren: () => import('./ion-progress-bar/ion-progress-bar.module').then( m => m.IonProgressBarPageModule)
  },
  {
    path: 'ion-skeleton-text',
    loadChildren: () => import('./ion-skeleton-text/ion-skeleton-text.module').then( m => m.IonSkeletonTextPageModule)
  },
  {
    path: 'ion-spinner',
    loadChildren: () => import('./ion-spinner/ion-spinner.module').then( m => m.IonSpinnerPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
