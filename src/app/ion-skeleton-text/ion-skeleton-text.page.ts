import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ion-skeleton-text',
  templateUrl: './ion-skeleton-text.page.html',
  styleUrls: ['./ion-skeleton-text.page.scss'],
})
export class IonSkeletonTextPage  implements OnInit {


  items: Array<any>;

  constructor() {}

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(): Promise<void> {
    setTimeout(() => {
      this.items = [
        { city: 'Berlin', country: 'Germany', population: '3.5 million', image: '../assets/image/Berlin.jpg' },
        { city: 'Buenos Aires', country: 'Argentina', population: '15 million', image: '../assets/image/ba.jpg' },
        { city: 'Madrid', country: 'Spain', population: '3.3 million', image: '../assets/image/madrid.jpg' }
      ];
    }, 2500);
  }
}
